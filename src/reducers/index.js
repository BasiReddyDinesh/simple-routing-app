const userReducer = (users = [], action) => {
  switch (action.type) {
    case "newUserList":
      return [...action.data];
    default:
      return users;
  }
};

export { userReducer };
