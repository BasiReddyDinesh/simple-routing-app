import { shallow } from "enzyme";
import { findElementByAttrs } from "../testUtils";

import UserDetails from "./UserDetails";

const setup = () => shallow(<UserDetails />);

it("renders UserDetails component without fail", () => {
  const wrapper = setup();
  const deatilsComp = findElementByAttrs(wrapper, "userDetails-component");
  expect(deatilsComp.exists()).toBeTruthy();
});
