import React from "react";
import Card from "../utils/Card";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";

import { fetchUsers } from "../actions";

function UserAccounts(props) {
  const dispatch = useDispatch();
  const users = useSelector((store) => store.users);
  React.useEffect(() => {
    dispatch(fetchUsers());
  }, []);
  return (
    <div data-test="userAccount-component">
      <Card list={users} />
    </div>
  );
}

UserAccounts.propTypes = {};

export default UserAccounts;
