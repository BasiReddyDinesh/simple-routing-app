import { mount } from "enzyme";
import { findElementByAttrs } from "../testUtils";
import { Provider } from "react-redux";
import store from "../store";
import UserAccounts from "./UserAccounts";
import { fetchUsers as mockFetchUsers } from "../actions";

jest.mock("../actions");

const setup = () =>
  mount(
    <Provider store={store}>
      <UserAccounts />
    </Provider>
  );

it("renders UserAccounts without fail", () => {
  const wrapper = setup();
  const userAccComp = findElementByAttrs(wrapper, "userAccount-component");
  expect(userAccComp.exists()).toBeTruthy();
});

describe("mockFetchUsers must calls once on mount", () => {
  beforeEach(() => {
    mockFetchUsers.mockClear();
  });
  it("mockFetchUsers must calls 1 on mount", () => {
    const wrapper = setup();
    expect(mockFetchUsers).toBeCalledTimes(1);
  });
  it("on update mockFetchUsers must not call", () => {
    const wrapper = setup();
    mockFetchUsers.mockClear();
    wrapper.setProps();
    expect(mockFetchUsers).toBeCalledTimes(0);
  });
});
