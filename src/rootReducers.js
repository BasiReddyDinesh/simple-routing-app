import { combineReducers } from "redux";
import { userReducer } from "./reducers/index";

export default combineReducers({
  users: userReducer,
});
