import axios from "axios";

const fetchUsers = () => {
  return (dispatch) => {
    return axios
      .get(`https://jsonplaceholder.typicode.com/users`)
      .then((response) => {
        dispatch({ type: "newUserList", data: response.data });
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

export { fetchUsers };
