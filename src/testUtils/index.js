import checkPropTypes from "check-prop-types";

const findElementByAttrs = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

const checkProps = (component, props) => {
  const propErr = checkPropTypes(
    component.PropTypes,
    props,
    "prop",
    component.name
  );
  expect(propErr).toBeUndefined();
};

export { findElementByAttrs, checkProps };
