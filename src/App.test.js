import { shallow } from "enzyme";
import { findElementByAttrs } from "./testUtils";
import App from "./App";

const setup = () => shallow(<App />);

test("renders App component without fail", () => {
  const wrapper = setup();
  const appComp = findElementByAttrs(wrapper, "app-component");
  expect(appComp.exists()).toBeTruthy();
});
