import { shallow } from "enzyme";
import { findElementByAttrs, checkProps } from "../testUtils";

import Card from "./Card";

const defaultProps = {
  list: [],
};

const setup = (props = { ...defaultProps }) =>
  shallow(<Card list={props.list} />);

it("renders Card component without fail", () => {
  const wrapper = setup();
  const cardComp = findElementByAttrs(wrapper, "card-component");
  expect(cardComp.exists()).toBeTruthy();
});

it("prop chould return undefined", () => {
  checkProps(Card, defaultProps);
});
