import React from "react";
import { mount } from "enzyme";
import store from "../store";
import { findElementByAttrs } from "../testUtils";
import { Provider } from "react-redux";
// import { fetchUsers as mockFetchUsers } from "../actions";

import App from "../App";

jest.mock("../actions");

const setup = () =>
  mount(
    <Provider store={store}>
      <App />
    </Provider>
  );

describe("does all components renders without fail", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = setup();
  });
  it("does App component renders without fail", () => {
    const appComp = findElementByAttrs(wrapper, "app-component");
    expect(appComp.exists()).toBeTruthy();
  });
  it("does useraccount component renders without fail", () => {
    const userAccComp = findElementByAttrs(wrapper, "userAccount-component");
    expect(userAccComp.exists()).toBeTruthy();
  });
  it("does card component renders without fail", () => {
    const cardComp = findElementByAttrs(wrapper, "card-component");
    expect(cardComp.exists()).toBeTruthy();
  });
});

describe("check the cards", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = setup();
  });
  it("cards comp len must be 3 initially", () => {
    const cardComp = findElementByAttrs(wrapper, "card-component");
    expect(cardComp.children()).toHaveLength(3);
  });
});
