import React from "react";
import store from "./store";
import { BrowserRouter, Route } from "react-router-dom";
import { Provider } from "react-redux";

import UserAccounts from "./components/UserAccounts";
import UserDetails from "./components/UserDetails";

import "./App.css";

function App() {
  return (
    <div data-test="app-component" className="App">
      <Provider store={store}>
        <BrowserRouter>
          <Route path="/" component={UserAccounts} exact />
          <Route path="/user/:id" component={UserDetails} exact />
        </BrowserRouter>
      </Provider>
    </div>
  );
}

export default App;
